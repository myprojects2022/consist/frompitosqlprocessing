﻿using System;
using System.Collections.Generic;
using Models.Data.Class;
using NLog;
using OSIsoft.AF.PI;
using OSIsoft.AF.Time;

namespace AfSdkRepository.Data.Class
{
    public class AfsdkInstance
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        public int TimeRange { get; set; }
        private PIServers Servers { get; set; }
        private AFTimeRange afTimeRange { get; set; }

        private readonly AfSdkData sdkData;

        public AfsdkInstance()
        {
            try
            {
                Servers = new PIServers();
                sdkData = new AfSdkData();
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось создать экземпляр класса. {ex.Message}");
                throw;
            }
        }
        public void Server(CalcObject calcObject, string Piserver)
        {
            try
            {
                calcObject.Server = Servers[Piserver];
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось получить PI SERVER {Piserver}. {ex.Message}");
                throw;
            }
        }

        public void Pipoint(CalcObject calcObject, string Tagname)
        {
            try
            {
                PIPoint pipoint = PIPoint.FindPIPoint(calcObject.Server, Tagname);
                calcObject.PointList.Add(Tagname, pipoint);
            }
            catch (Exception ex)
            {
                logger.Warn($"Не удалось получить Тэг {Tagname} или добавить его в объект расчета.{ex.Message}");
                throw;
            }
        }

        public void Calculate(Dictionary<string, CalcObject> CalcObjects, DateTime DateTime)
        {
            try
            {
                afTimeRange = new AFTimeRange(new AFTime(DateTime.AddSeconds(-TimeRange)), new AFTime(DateTime));
                foreach (var item in CalcObjects)
                {
                    sdkData.Data(item.Value, afTimeRange);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка при выполнении метода расчета - получения данных из PI. {ex.Message}");
                throw;
            }
        }
    }
}
