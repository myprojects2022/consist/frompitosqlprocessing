﻿using System;
using Models.Data.Class;
using NLog;
using OSIsoft.AF.Asset;
using OSIsoft.AF.PI;
using OSIsoft.AF.Time;


namespace AfSdkRepository.Data.Class
{
    public class AfSdkData
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private string Mode { get; set; }

        public void Data(CalcObject calcObject, AFTimeRange afTimeRange)
        {
            Mode = calcObject.Mode;
            try
            {
                foreach (var item in calcObject.PointList)
                {
                    AFValues Values = GetAFvalues(item.Value, afTimeRange);
                    if (Values.Count > 0)
                    {
                        calcObject.Timeseries[item.Key] = MapTimeSeries(Values);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка итераций. {ex.Message}");
                throw;
            }
        }
        private TimeSeries MapTimeSeries(AFValues aFValues)
        {
            var Timeseries = new TimeSeries();
            try
            {
                foreach (var pivalue in aFValues)
                {
                    TagValues tagValues = new TagValues()
                    {
                        TimeStamp = pivalue.Timestamp.LocalTime,
                        Value = Convert.ToDouble(pivalue.Value)
                    };
                    Timeseries.TagValue.Add(tagValues);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка заполнения объекта расчета данными из тэгов. {ex.Message}");
                throw;
            }
            return Timeseries;
        }

        private AFValues GetAFvalues(PIPoint point, AFTimeRange afTimeRange)
        {
            AFValues AFValues = new AFValues();
            try
            {
                if (Mode == "Full")
                {
                    AFValues = point.RecordedValues(afTimeRange, OSIsoft.AF.Data.AFBoundaryType.Inside, null, false);
                }
                else
                {
                    AFValues.Add(point.RecordedValue(afTimeRange.EndTime, OSIsoft.AF.Data.AFRetrievalMode.AtOrBefore));
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка получения данных из тэгов. {ex.Message}");
                throw;
            }
            return AFValues;
        }
    }
}
