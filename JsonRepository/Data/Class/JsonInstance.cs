﻿using System;
using System.Collections.Generic;
using System.IO;
using Models.Data.Class;
using Newtonsoft.Json;
using NLog;

namespace JsonRepository.Data.Class
{
    public class JsonInstance
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private RootObjectConfig Root { get; set; }


        private void Collection(string FileName)
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                Root = JsonConvert.DeserializeObject<RootObjectConfig>(reader.ReadToEnd());
            };
        }

        public Dictionary<string, CalcObjectConfig> CollectObjects(string FileName)
        {
            logger.Info("Сборка Json");
            var obj = new Dictionary<string, CalcObjectConfig>();
            try
            {
                Collection(FileName);
                foreach (var item in Root.CalcObjectConfig)
                {
                    obj.Add(item.Number.ToString(), item);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось получить коллекцию Json. {ex.Message}");
                throw;
            }
            return obj;
        }

    }
}
