﻿using FromPItoSQLProcessing.Data.Class;
using System;

namespace TestingConsole
{
    class Program
    {
        private static Processing processing;
        static void Main()
        {
            Initialize();
            Calulate();
        }
        static void Initialize()
        {
            processing = new Processing();
        }

        static void Calulate()
        {
            DateTime dt = DateTime.Now;
            processing.Calculate(dt);
        }
    }
}
