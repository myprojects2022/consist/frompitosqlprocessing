﻿using System;
using System.Collections.Generic;
using OSIsoft.AF.PI;
namespace Models.Data.Class
{
    public class CalcObject
    {
        public string Name { get; set; }
        public string ConnString { get; set; }
        public string Table { get; set; }
        public string Mode { get; set; }
        public PIServer Server { get; set; }
        public Dictionary<string, PIPoint> PointList { get; set; } //коллекция тэгов
        public Dictionary<string, TimeSeries> Timeseries { get; set; }// колллекция данных по тэгам

        public CalcObject()
        {
            Timeseries = new Dictionary<string, TimeSeries>();
            PointList = new Dictionary<string, PIPoint>();
        }

    }
}
