﻿using System.Collections.Generic;

namespace Models.Data.Class
{
    public class CalcObjectConfig
    {
        public int Number { get; set; }
        public string SQLServer { get; set; }
        public string DataBaseName { get; set; }
        public string TableName { get; set; }
        public string PIServerName { get; set; }
        public string Mode { get; set; }
        public List<string> TagArray { get; set; }

        public CalcObjectConfig()
        {
            TagArray = new List<string>();
        }
    }
}
