﻿using System.Collections.Generic;

namespace Models.Data.Class
{
    public class RootObjectConfig
    {
        public List<CalcObjectConfig> CalcObjectConfig { get; set; }
        public RootObjectConfig()
        {
            CalcObjectConfig = new List<CalcObjectConfig>();
        }

    }
}
