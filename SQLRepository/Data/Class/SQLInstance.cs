﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models.Data.Class;

namespace SQLRepository.Data.Class
{
    public class SQLInstance
    {
        private string ConnString { get; set; }
        private string TableName { get; set; }


        public void Calculate(Dictionary<string, CalcObject> CalcObjects)
        {
            foreach (var item in CalcObjects)
            {
                Data(item.Value);
            }
        }
        private void Data(CalcObject calcObject)
        {
            var mode = calcObject.Mode;
            ConnString = calcObject.ConnString;
            TableName = calcObject.Table;

            foreach (var item in calcObject.Timeseries)
            {
                if (mode == "ReplaceSnapshot")
                {
                    DatUpdate(item.Value.TagValue, item.Key, mode);
                }
                else
                {
                    DataInsert(item.Value.TagValue,item.Key, mode);
                }
                item.Value.TagValue.Clear();
            }
        }

        private void DataInsert(List<TagValues> tagValues,string TagName,string Mode)
        {
            using (IDbConnection db = new SqlConnection(ConnString))
            {
                var query = $"INSERT INTO {TableName} (TagName,Value,Timestamp,WriteMode) Values ('{TagName}',@Value, @TimeStamp,'{Mode}')";
                db.Execute(query, tagValues);
            }
        }

        private void DatUpdate(List<TagValues> tagValues, string TagName, string Mode)
        {
           using (IDbConnection db = new SqlConnection(ConnString))
            {
                var query = $"exec dbo.MergePIData '{TagName}', @Value, @TimeStamp,'{Mode}'";
                db.Execute(query, tagValues);
            }

        }
    }
}
