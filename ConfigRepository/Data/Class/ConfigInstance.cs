﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;


namespace ConfigRepository.Data.Class
{
    public class ConfigInstance
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        public IReadOnlyDictionary<string, string> InstanceSettings(string exeConfigPath)
        {
            var ConfigDict = new Dictionary<string, string>();
            logger.Info("Сбор конфигурационных параметров");
            try
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
                foreach (KeyValueConfigurationElement item in configuration.AppSettings.Settings)
                {
                    ConfigDict.Add(item.Key, item.Value);
                }
            }
            catch (Exception ex)
            {
                logger.Error($"Не удалось собрать конфигурацию. Ошибка {ex.Message}");
                throw;
            }
            return ConfigDict;
        }


    }
}
