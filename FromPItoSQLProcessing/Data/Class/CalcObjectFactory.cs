﻿using AfSdkRepository.Data.Class;
using Models.Data.Class;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FromPItoSQLProcessing.Data.Class
{
    public class CalcObjectFactory
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private AfsdkInstance AfsdkInstance { get; set; }
        public CalcObjectFactory(AfsdkInstance afsdkInstance)
        {
            this.AfsdkInstance = afsdkInstance;
        }

        public Dictionary<string, CalcObject> GetCollection(Dictionary<string, CalcObjectConfig> CalcObjectConfigs)
        {
            try
            {
                logger.Info("Запуск сбора коллекции");
                var calcObject = new Dictionary<string, CalcObject>();

                foreach (var item in CalcObjectConfigs)
                {
                    var obj = Create(item.Value);
                    calcObject.Add(item.Key, obj);
                    logger.Info($"Добавлен объект с именем {item.Key}");
                }
                return calcObject;
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка сбора коллекции. {ex.Message}");
                throw;
            }
        }
        private CalcObject Create(CalcObjectConfig calcObjectConfig)
        {
            var obj = new CalcObject
            {
                Name = calcObjectConfig.Number.ToString(),
                ConnString = ($"Data Source={calcObjectConfig.SQLServer};Initial Catalog={calcObjectConfig.DataBaseName};Persist Security Info=True;Integrated Security=SSPI"),
                Table = calcObjectConfig.TableName,
                Mode = calcObjectConfig.Mode
            };
            AfsdkInstance.Server(obj, calcObjectConfig.PIServerName);
            MapTagList(obj, calcObjectConfig.TagArray);
            return obj;
        }
        private void MapTagList(CalcObject obj, List<string> TagList)
        {
            foreach (var item in TagList)
            {
                obj.Timeseries.Add(item, new TimeSeries());
                AfsdkInstance.Pipoint(obj, item);
            }
        }


        //продумать
        public void CheckUpdateDelete(Dictionary<string, CalcObject> CalcObjects, Dictionary<string, CalcObjectConfig> CalcObjectConfigs)
        {
            var objects = CalcObjects.Keys.ToList();
            var configs = CalcObjectConfigs.Keys.ToList();

            foreach (var item in configs)
            {
                if (!objects.Contains(item))
                {            
                    var obj = Create(CalcObjectConfigs[item]);
                    CalcObjects.Add(item, obj);
                    logger.Info($"Обнаружено изменение JSON, Добавлен объект с именем {item}");
                }
            }

            foreach (var item in objects)
            {
                if (!configs.Contains(item)) CalcObjects.Remove(item);
                logger.Info($"Обнаружено изменение JSON, Удален объект с именем {item}");
            }

            //проверка количества тэгов
            foreach (var item in CalcObjects)
            {
                var objectTags = item.Value.Timeseries.Keys.ToList();
                var configTags = CalcObjectConfigs[item.Key].TagArray.ToList();
                bool Equal = objectTags.SequenceEqual(configTags);
                
                if (!Equal)
                {

                    MapTagList(item.Value, CalcObjectConfigs[item.Key].TagArray);
                    logger.Info($"Обнаружено изменение JSON, обновление списка тэгов объекта с именем {item.Key}");
                }
            }
        }
    }

}
