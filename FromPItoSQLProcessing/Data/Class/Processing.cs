﻿using System;
using System.Collections.Generic;
using AfSdkRepository.Data.Class;
using ConfigRepository.Data.Class;
using JsonRepository.Data.Class;
using Models.Data.Class;
using SQLRepository.Data.Class;
using NLog;

namespace FromPItoSQLProcessing.Data.Class
{

    public  class Processing
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ConfigInstance configInstance;
        private readonly JsonInstance jsonInstance;
        private readonly SQLInstance sqlInstance;
        private readonly CalcObjectFactory CalcObjectFactory;
        private readonly AfsdkInstance afsdkInstance;

        private IReadOnlyDictionary<string, string> Config { get; set; }
        private Dictionary<string, CalcObjectConfig> CalcObjectConfigs { get; set; }
        private Dictionary<string, CalcObject> CalcObjects { get; set; }
       

        public Processing()
        {
            try
            {
                logger.Info("Сборка");
                configInstance = new ConfigInstance();
                jsonInstance = new JsonInstance();
                sqlInstance = new SQLInstance();
                afsdkInstance = new AfsdkInstance();
                CalcObjectFactory = new CalcObjectFactory(afsdkInstance);
                Initialize();
            }
            catch (Exception ex)
            {
                logger.Error($"Сборка не выполнена. {ex.Message}");
                throw;
            }
        }
        private void Initialize()
        {
            try
            {
                Config = configInstance.InstanceSettings(GetType().Assembly.Location);
                CalcObjectConfigs = jsonInstance.CollectObjects(Config["JsonFileName"]);
                CalcObjects = CalcObjectFactory.GetCollection(CalcObjectConfigs);
                afsdkInstance.TimeRange = int.Parse(Config["TimeRange"]);
            }
            catch (Exception ex)
            {
                logger.Error($"Ошибка инициализации расчета. {ex.Message}");
                throw;
            }
        }

        public void Calculate(DateTime DateTime)
        {
            logger.Trace($"Запуск расчета. задано время {DateTime}");
            //CheckChanges();
            afsdkInstance.Calculate(CalcObjects,DateTime);
            sqlInstance.Calculate(CalcObjects);
        }

        private void CheckChanges()
        {            
        }
    }
}
